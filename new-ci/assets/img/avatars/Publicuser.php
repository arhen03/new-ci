<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use \Firebase\JWT\JWT;

class Publicuser extends CI_Controller
{
    private $key = '5821b77d224cc789449142a72982db50'; // biasamatamantap

    public function __construct()
    {
        parent::__construct();
        $this->load->library('php_jwt');
    }

    public function index() {
        echo 'E-lAW API';
    }

    public function dashboard(){
        $time_start = microtime(true);
        $response = array();
        $data = array();

        $required = array(
            'authorization' => array('mandatory' => true),
            'page' => array('mandatory' => false, "type" => 'text', 'example' => '1', 'fieldtext' => 'page'));

        $i = $this->input;
        if ($this->is_token_valid()) {
            $where = array();

            $page = $i->get_request_header('page') != NULL ? (int)$i->get_request_header('page') : 1;

            $offset = $page == 1 ? 0 : ((10 * $page) - 50);

            $rows  = $this->crud->ca('categorylist');
            if ($rows == null) $rows = 0;
            $data = $this->crud->gl('categorylist',50,$offset);

            $response['diagnostic']['status'] = 200;
            $response['diagnostic']['success'] = TRUE;
            $response['diagnostic']['message'] = 'Success!';
            $response['pagination'] = array(
                    'total_data'    => $rows,
                    'limit_page'    => 50,
                    'active_page'   => $i->get_request_header('page') != NULL ? (int)$i->get_request_header('page') : 1,
                    'total_page'    => ceil($rows/50));

            $response['response'] = $data;
        } else {
            $response['diagnostic']['status'] = 401;
            $response['diagnostic']['success'] = FALSE;
            $response['diagnostic']['message'] = 'Invalid Token !';
            $response['response'] = array('valid' => FALSE);
        }

        $time_end = microtime(true);
        $execution_time = ($time_end - $time_start)/60;
        $response['diagnostic']['elapsed_time'] = ''.$execution_time;
        $response['required'] = $required;

        return $this->response($response);
    }

    public function categorySpecialist(){
        $time_start = microtime(true);
        $response = array();
        $data = array();

        $required = array(
            'authorization' => array('mandatory' => true),
            'categoryId' => array('mandatory' => true, "type" => 'text', 'example' => 6, 'fieldtext' => 'categoryId'),
            'page' => array('mandatory' => false, "type" => 'text', 'example' => '1', 'fieldtext' => 'page'));

        $i = $this->input;
        $categoryId  = $i->get_request_header('categoryId');
        if ($this->is_token_valid()) {
            if ($categoryId == NULL) {
                $response['diagnostic']['status'] = 401;
                $response['diagnostic']['success'] = FALSE;
                $response['diagnostic']['message'] = 'Invalid parameter values!';
                $response['response'] = array('valid' => FALSE);
            }else{
                $where = array('categoryId' => $categoryId);

                $page = $i->get_request_header('page') != NULL ? (int)$i->get_request_header('page') : 1;

                $offset = $page == 1 ? 0 : ((50 * $page) - 50);

                $rows  = $this->crud->cw('expertspecialistlist',$where);
                if ($rows == null) $rows = 0;
                $data = $this->crud->gwl('expertspecialistlist',$where,50,$offset);
                if ($data != NULL) {
                    foreach ($data as $key) {
                        $data_expert = $this->crud->gda('expertuser',array('expertUserId' => $key->expertUserId));
                        $key->categoryId = intVal($key->categoryId);
                        $key->expertSpecialistId = intVal($key->expertSpecialistId);
                        $key->expertFullname = $data_expert['expertFullname'];
                        $key->expertLawCode = intVal($data_expert['expertLawCode']);
                        $key->expertPhotoProfile= $data_expert['expertPhotoProfile'];
                    }

                }else $data = 0;



                $response['diagnostic']['status'] = 200;
                $response['diagnostic']['success'] = TRUE;
                $response['diagnostic']['message'] = 'Success!';
                $response['pagination'] = array(
                        'total_data'    => $rows,
                        'limit_page'    => 50,
                        'active_page'   => $i->get_request_header('page') != NULL ? (int)$i->get_request_header('page') : 1,
                        'total_page'    => ceil($rows/50));

                $response['response'] = $data;
            }

        } else {
            $response['diagnostic']['status'] = 401;
            $response['diagnostic']['success'] = FALSE;
            $response['diagnostic']['message'] = 'Invalid Token !';
            $response['response'] = array('valid' => FALSE);
        }

        $time_end = microtime(true);
        $execution_time = ($time_end - $time_start)/60;
        $response['diagnostic']['elapsed_time'] = ''.$execution_time;
        $response['required'] = $required;

        return $this->response($response);
    }

    public function expertProfile(){
        $time_start = microtime(true);
        $response = array();
        $data = array();

        $required = array(
            'authorization' => array('mandatory' => true),
            'expertUserId' => array('mandatory' => true, "type" => 'text', 'example' => 'E-001', 'fieldtext' => 'expertUserId'));

        $i = $this->input;
        if ($this->is_token_valid()) {
            $expertUserId  = $i->get_request_header('expertUserId');
            if ($expertUserId == NULl) {
                $response['diagnostic']['status'] = 401;
                $response['diagnostic']['success'] = FALSE;
                $response['diagnostic']['message'] = 'Invalid parameter values!';
                $response['response'] = array('valid' => FALSE);
            }else{
                $where = array('expertUserId' => $expertUserId);

                $data['data_expert'] = $this->crud->gda('expertuser',$where);
                $data['data_expert']['expertLawCode'] = intVal($data['data_expert']['expertLawCode']);
                $data['data_expert']['accountStatus'] = intVal($data['data_expert']['accountStatus']);
                if ($data['data_expert']['userType'] == 'lawyer') {
                    $data['data_expert']['specialist'] = $this->crud->qwo("SELECT categoryName from categorylist inner join expertspecialistlist on expertspecialistlist.categoryId = categorylist.categoryId where expertspecialistlist.expertUserId = '$expertUserId'");
                    foreach (json_decode(json_encode($data['data_expert']['specialist']),true) as $value) {
                        $array[] = $value['categoryName'];
                    }
                    $data['data_expert']['specialist'] = implode(',',$array);
                }
                $expertUserId = $data['data_expert']['expertUserId'];
                $data['data_expert']['expertPhotoProfile'] = 'https://api.biasamata.com/e-law/public/img/uploads/userpics/' . $data['data_expert']['expertPhotoProfile'];
                $data['data_problemSolved'] = $this->crud->gw('problemsolvedlist',$where) != NULL ? $this->crud->gw('problemsolvedlist',$where) : 0;
                $data['data_problemSolved'] = $data['data_problemSolved'] != NULL ? $data['data_problemSolved'] : 0;
                foreach ($data['data_problemSolved'] as $value) {
                    // $value->date = date('d M Y',$value->iat);
                    // var_dump(strtotime($value->iat));
                    // die();
                    unset($value->expertUserId,$value->iat,$value->uat);
                }


                $response['diagnostic']['status'] = 200;
                $response['diagnostic']['success'] = TRUE;
                $response['diagnostic']['message'] = 'Success!';
                $response['response'] = $data;
            }
        } else {
            $response['diagnostic']['status'] = 401;
            $response['diagnostic']['success'] = FALSE;
            $response['diagnostic']['message'] = 'Invalid Token !';
            $response['response'] = array('valid' => FALSE);
        }

        $time_end = microtime(true);
        $execution_time = ($time_end - $time_start)/60;
        $response['diagnostic']['elapsed_time'] = ''.$execution_time;
        $response['required'] = $required;

        return $this->response($response);
    }

    public function lawyerInfo(){
        $time_start = microtime(true);
        $response = array();
        $data = array();

        $required = array(
            'authorization' => array('mandatory' => true),
            'expertUserId' => array('mandatory' => true, "type" => 'text', 'example' => 'E-001', 'fieldtext' => 'expertUserId'));

        $i = $this->input;
        if ($this->is_token_valid()) {
            $expertUserId  = $i->get_request_header('expertUserId');
            if ($expertUserId == NULl) {
                $response['diagnostic']['status'] = 401;
                $response['diagnostic']['success'] = FALSE;
                $response['diagnostic']['message'] = 'Invalid parameter values!';
                $response['response'] = array('valid' => FALSE);
            }else{
                $where = array('experUserId' => $experUserId);

                $data_profil = $this->crud->gda("expertuser", $where);
                $data_profil->expertPhotoProfile = 'https://api.biasamata.com/e-law/public/img/uploads/userpics/'.$data_profil->expertPhotoProfile;
                $data_specialist = $this->crud->qwo("SELECT categoryName from categorylist inner join expertspecialistlist on expertspecialistlist.categoryId = categorylist.categoryId where categorylist.expertUserId = '$expertUserId'");
                $data_problemSolved = $this->crud->gda('problemsolvedlist',$where);

                $data = array(
                    'data_profil' => $data_profil,
                    'data_specialist' => $data_specialist,
                    'data_problemSolved' => $data_problemSolved);

                $response['diagnostic']['status'] = 200;
                $response['diagnostic']['success'] = TRUE;
                $response['diagnostic']['message'] = 'Success!';
                $response['response'] = $data;
            }
        } else {
            $response['diagnostic']['status'] = 401;
            $response['diagnostic']['success'] = FALSE;
            $response['diagnostic']['message'] = 'Invalid Token !';
            $response['response'] = array('valid' => FALSE);
        }

        $time_end = microtime(true);
        $execution_time = ($time_end - $time_start)/60;
        $response['diagnostic']['elapsed_time'] = ''.$execution_time;
        $response['required'] = $required;

        return $this->response($response);
    }

    public function questionDetail(){
        $time_start = microtime(true);
        $response = array();
        $data = array();

        $required = array(
            'authorization' => array('mandatory' => true),
            'questionId' => array('mandatory' => true, "type" => 'text', 'example' => '1', 'fieldtext' => 'questionId'));

        $i = $this->input;
        $questionId  = $i->get_request_header('questionId');
        if ($this->is_token_valid()) {
            if ($questionId == NULL) {
                $response['diagnostic']['status'] = 401;
                $response['diagnostic']['success'] = FALSE;
                $response['diagnostic']['message'] = 'Invalid parameter values!';
                $response['response'] = array('valid' => FALSE);
            }else{
                $where = array('publicUserId' => $publicUserId);

                $data['questionDetail'] = $this->crud->gda('questiontoexpertlist',$where);
                $data['questionDetail']['questionId'] = intVal($data['questionDetail']['questionId']);
                $publicData = $this->crud->gda('publicuser',array('publicUserId' => $data['questionDetail']['publicUserId']));
                $data['questionDetail']['questionFullname'] = $publicData['publicFullname'];
                $data['questionDetail']['questionPhotoProfile'] = 'https://api.biasamata.com/e-law/public/img/uploads/userpics/'.$publicData['publicPhotoProfile'];

                $data['answerList'] = $this->crud->gw('answerbyexpertlist',$where);
                foreach ($data['answerList'] as $value) {
                    $value->answerId = intVal($value->answerId);
                    $expertData = $this->crud->gda('expertuser',array('expertUserId' => $value->expertUserId));
                    $value->answerFullname = $expertData['expertFullname'];
                    $value->answerPhotoProfile = 'https://api.biasamata.com/e-law/public/img/uploads/userpics/'.$expertData['expertPhotoProfile'];
                    unset($value->answerVoted,$value->questionId);
                }

                $response['diagnostic']['status'] = 200;
                $response['diagnostic']['success'] = TRUE;
                $response['diagnostic']['message'] = 'Success!';
                $response['response'] = $data;
            }

        } else {
            $response['diagnostic']['status'] = 401;
            $response['diagnostic']['success'] = FALSE;
            $response['diagnostic']['message'] = 'Invalid Token !';
            $response['response'] = array('valid' => FALSE);
        }

        $time_end = microtime(true);
        $execution_time = ($time_end - $time_start)/60;
        $response['diagnostic']['elapsed_time'] = ''.$execution_time;
        $response['required'] = $required;

        return $this->response($response);
    }

    public function question_get(){
        $time_start = microtime(true);
        $response = array();
        $data = array();

        $required = array(
            'authorization' => array('mandatory' => true),
            'publicUserId' => array('mandatory' => true, "type" => 'text', 'example' => 'P-001', 'fieldtext' => 'publicUserId'),
            'page' => array('mandatory' => false, "type" => 'text', 'example' => '1', 'fieldtext' => 'page'));

        $i = $this->input;
        $publicUserId  = $i->get_request_header('publicUserId');
        if ($this->is_token_valid()) {
            if ($publicUserId == NULL) {
                $response['diagnostic']['status'] = 401;
                $response['diagnostic']['success'] = FALSE;
                $response['diagnostic']['message'] = 'Invalid parameter values!';
                $response['response'] = array('valid' => FALSE);
            }else{
                $where = array();

                $page = $i->get_request_header('page') != NULL ? (int)$i->get_request_header('page') : 1;

                $offset = $page == 1 ? 0 : ((10 * $page) - 50);

                $rows  = $this->crud->ca('questiontoexpertlist');
                if ($rows == null) $rows = 0;
                $data['questionList'] = $this->crud->glo('questiontoexpertlist',50,$offset,'questionId DESC');
                foreach ($data['questionList'] as $value) {
                    $value->questionId = intVal($value->questionId);
                    $publicData = $this->crud->gda('publicuser',array('publicUserId' => $value->publicUserId));
                    $value->questionFullname = $publicData['publicFullname'] != null ? $publicData['publicFullname'] : 0;
                    $value->questionPhotoProfile = $publicData['publicPhotoProfile'] != NULL ? 'https://api.biasamata.com/e-law/public/img/uploads/userpics/'.$publicData['publicPhotoProfile'] : 0;
                    $value->canDelete = $value->publicUserId == $publicUserId ? TRUE : FALSE;
                }

                $data['publicPhotoProfile'] = $this->crud->gda('publicuser',array('publicUserId' => $publicUserId));
                $data['publicPhotoProfile'] = 'https://api.biasamata.com/e-law/public/img/uploads/userpics/'.$data['publicPhotoProfile']['publicPhotoProfile'];

                $response['diagnostic']['status'] = 200;
                $response['diagnostic']['success'] = TRUE;
                $response['diagnostic']['message'] = 'Success!';
                $response['pagination'] = array(
                        'total_data'    => $rows,
                        'limit_page'    => 50,
                        'active_page'   => $i->get_request_header('page') != NULL ? (int)$i->get_request_header('page') : 1,
                        'total_page'    => ceil($rows/50));

                $response['response'] = $data;
            }

        } else {
            $response['diagnostic']['status'] = 401;
            $response['diagnostic']['success'] = FALSE;
            $response['diagnostic']['message'] = 'Invalid Token !';
            $response['response'] = array('valid' => FALSE);
        }

        $time_end = microtime(true);
        $execution_time = ($time_end - $time_start)/60;
        $response['diagnostic']['elapsed_time'] = ''.$execution_time;
        $response['required'] = $required;

        return $this->response($response);
    }

    public function question_post(){
        $time_start = microtime(true);
        $response = array();
        $data = array();

        $required = array(
            'authorization' => array('mandatory' => true),
            'publicUserId' => array('mandatory' => true, "type" => 'text', 'example' => 'P-001', 'fieldtext' => 'publicUserId'),
            'description' => array('mandatory' => true, "type" => 'text', 'example' => 'testing!', 'fieldtext' => 'description'));

        $i = $this->input;
        $publicUserId  = $i->get_request_header('publicUserId');
        $description  = $i->get_request_header('description');
        if ($this->is_token_valid()) {
            if ($publicUserId == NULL OR $description == NULL) {
                $response['diagnostic']['status'] = 401;
                $response['diagnostic']['success'] = FALSE;
                $response['diagnostic']['message'] = 'Invalid parameter values!';
                $response['response'] = array('valid' => FALSE);
            }else{
                $date = new DateTime();
                $iat = $date->getTimestamp();
                $data = array(
                    'questionDescription' => $description,
                    'publicUserId' => $publicUserId,
                    'iat' => $iat
                );
                $this->crud->i('questiontoexpertlist',$data);

                $response['diagnostic']['status'] = 200;
                $response['diagnostic']['success'] = TRUE;
                $response['diagnostic']['message'] = 'Success add Question!';
            }

        } else {
            $response['diagnostic']['status'] = 401;
            $response['diagnostic']['success'] = FALSE;
            $response['diagnostic']['message'] = 'Invalid Token !';
            $response['response'] = array('valid' => FALSE);
        }

        $time_end = microtime(true);
        $execution_time = ($time_end - $time_start)/60;
        $response['diagnostic']['elapsed_time'] = ''.$execution_time;
        $response['required'] = $required;

        return $this->response($response);
    }

    public function question_del(){
        $time_start = microtime(true);
        $response = array();
        $data = array();

        $required = array(
            'authorization' => array('mandatory' => true),
            'questionId' => array('mandatory' => true, "type" => 'text', 'example' => '1', 'fieldtext' => 'questionId'));

        $i = $this->input;
        $questionId  = $i->get_request_header('questionId');
        if ($this->is_token_valid()) {
            if ($questionId == NULL) {
                $response['diagnostic']['status'] = 401;
                $response['diagnostic']['success'] = FALSE;
                $response['diagnostic']['message'] = 'Invalid parameter values!';
                $response['response'] = array('valid' => FALSE);
            }else{

                $this->crud->d('questiontoexpertlist',array('questionId' => $questionId));
                $this->crud->d('answerbyexpertlist',array('questionId' => $questionId));

                $response['diagnostic']['status'] = 200;
                $response['diagnostic']['success'] = TRUE;
                $response['diagnostic']['message'] = 'Success delete Question!';
            }

        } else {
            $response['diagnostic']['status'] = 401;
            $response['diagnostic']['success'] = FALSE;
            $response['diagnostic']['message'] = 'Invalid Token !';
            $response['response'] = array('valid' => FALSE);
        }

        $time_end = microtime(true);
        $execution_time = ($time_end - $time_start)/60;
        $response['diagnostic']['elapsed_time'] = ''.$execution_time;
        $response['required'] = $required;

        return $this->response($response);
    }

    public function searchProblemName(){
        $time_start = microtime(true);
        $response = array();
        $data = array();

        $required = array(
            'authorization' => array('mandatory' => true),
            'keyword' => array('mandatory' => true, "type" => 'text', 'example' => 'P-001', 'fieldtext' => 'keyword'),
            'page' => array('mandatory' => false, "type" => 'text', 'example' => '1', 'fieldtext' => 'page'));

        $i = $this->input;
        $keyword  = $i->get_request_header('keyword');
        if ($this->is_token_valid()) {
            if ($keyword == NULL) {
                $response['diagnostic']['status'] = 401;
                $response['diagnostic']['success'] = FALSE;
                $response['diagnostic']['message'] = 'Invalid parameter values!';
                $response['response'] = array('valid' => FALSE);
            }else{
                $page = $i->get_request_header('page') != NULL ? (int)$i->get_request_header('page') : 1;

                $offset = $page == 1 ? 0 : ((10 * $page) - 50);

                $rows  = $this->crud->wl('problemsolvedlist','problemSolvedTitle',$keyword);
                if ($rows == null) $rows = 0;
                $data = $this->crud->wlo('problemsolvedlist','problemSolvedTitle',$keyword);
                foreach ($data as $value) {
                    $data_expert = $this->crud->gda('expertuser',array('expertUserId' => $value->expertUserId));
                    $value->expertFullname = $data_expert['expertFullname'];
                }

                $response['diagnostic']['status'] = 200;
                $response['diagnostic']['success'] = TRUE;
                $response['diagnostic']['message'] = 'Success!';
                $response['pagination'] = array(
                        'total_data'    => $rows,
                        'limit_page'    => 50,
                        'active_page'   => $i->get_request_header('page') != NULL ? (int)$i->get_request_header('page') : 1,
                        'total_page'    => ceil($rows/50));

                $response['response'] = $data;
            }

        } else {
            $response['diagnostic']['status'] = 401;
            $response['diagnostic']['success'] = FALSE;
            $response['diagnostic']['message'] = 'Invalid Token !';
            $response['response'] = array('valid' => FALSE);
        }

        $time_end = microtime(true);
        $execution_time = ($time_end - $time_start)/60;
        $response['diagnostic']['elapsed_time'] = ''.$execution_time;
        $response['required'] = $required;

        return $this->response($response);
    }

    public function lawArticles(){
        $time_start = microtime(true);
        $response = array();
        $data = array();

        $required = array(
            'authorization' => array('mandatory' => true),
            'page' => array('mandatory' => false, "type" => 'text', 'example' => '1', 'fieldtext' => 'page'));

        $i = $this->input;
        if ($this->is_token_valid()) {

            $page = $i->get_request_header('page') != NULL ? (int)$i->get_request_header('page') : 1;

            $offset = $page == 1 ? 0 : ((10 * $page) - 100);

            $rows  = $this->crud->ca('lawarticles');
            if ($rows == null) $rows = 0;
            $data = $this->crud->glo('lawarticles',100,$offset,'articleId DESC');
            foreach ($data as $value) {
                $value->articleId = intVal($value->articleId);
                $value->articleImage =  'https://api.biasamata.com/e-law/public/img/uploads/articlepics/'.$value->articleImage;
                $expertData = $this->crud->gda('expertuser',array('expertUserId' => $value->expertUserId));
                $value->authorFullname = $expertData['expertFullname'] != null ? $expertData['expertFullname'] : 0;

                //Create Date
                $format = 'Y-m-d H:i:s';
                $date = DateTime::createFromFormat($format, $value->iat);
                $value->iat =  $date->format('D, d M Y, H:i:s');

                unset($value->articleDescription, $value->articleImage, $value->uat);
            }

            $response['diagnostic']['status'] = 200;
            $response['diagnostic']['success'] = TRUE;
            $response['diagnostic']['message'] = 'Success!';
            $response['pagination'] = array(
                    'total_data'    => $rows,
                    'limit_page'    => 100,
                    'active_page'   => $i->get_request_header('page') != NULL ? (int)$i->get_request_header('page') : 1,
                    'total_page'    => ceil($rows/100));

            $response['response'] = $data;
        } else {
            $response['diagnostic']['status'] = 401;
            $response['diagnostic']['success'] = FALSE;
            $response['diagnostic']['message'] = 'Invalid Token !';
            $response['response'] = array('valid' => FALSE);
        }

        $time_end = microtime(true);
        $execution_time = ($time_end - $time_start)/60;
        $response['diagnostic']['elapsed_time'] = ''.$execution_time;
        $response['required'] = $required;

        return $this->response($response);
    }

    public function lawArticleDetail(){
        $time_start = microtime(true);
        $response = array();
        $data = array();

        $required = array(
            'authorization' => array('mandatory' => true),
            'articleId' => array('mandatory' => true, "type" => 'text', 'example' => '1', 'fieldtext' => 'articleId'));

        $i = $this->input;
        $articleId  = $i->get_request_header('articleId');
        if ($this->is_token_valid()) {
            if ($articleId == NULL) {
                $response['diagnostic']['status'] = 401;
                $response['diagnostic']['success'] = FALSE;
                $response['diagnostic']['message'] = 'Invalid parameter values!';
                $response['response'] = array('valid' => FALSE);
            }else{
                $where = array('articleId' => $articleId);

                $data = $this->crud->glo('lawarticles',$where);

                foreach ($data as $value) {
                    $value->articleId = intVal($value->articleId);
                    $value->articleImage =  'https://api.biasamata.com/e-law/public/img/uploads/articlepics/'.$value->articleImage;
                    $expertData = $this->crud->gda('expertuser',array('expertUserId' => $value->expertUserId));
                    $value->authorFullname = $expertData['expertFullname'] != null ? $expertData['expertFullname'] : 0;

                    //Create Date
                    $format = 'Y-m-d H:i:s';
                    $date = DateTime::createFromFormat($format, $value->iat);
                    $value->iat =  $date->format('D, d M Y, H:i:s');

                    unset($value->uat);
                }

                $response['diagnostic']['status'] = 200;
                $response['diagnostic']['success'] = TRUE;
                $response['diagnostic']['message'] = 'Success!';
                $response['response'] = $data;
            }

        } else {
            $response['diagnostic']['status'] = 401;
            $response['diagnostic']['success'] = FALSE;
            $response['diagnostic']['message'] = 'Invalid Token !';
            $response['response'] = array('valid' => FALSE);
        }

        $time_end = microtime(true);
        $execution_time = ($time_end - $time_start)/60;
        $response['diagnostic']['elapsed_time'] = ''.$execution_time;
        $response['required'] = $required;

        return $this->response($response);
    }

    public function accountInfo_get(){
        $time_start = microtime(true);
        $response = array();
        $data = array();

        $required = array(
            'authorization' => array('mandatory' => true),
            'username' => array('mandatory' => true, "type" => 'text', 'example' => 'P-001', 'fieldtext' => 'username'));

        $i = $this->input;
        if ($this->is_token_valid()) {
            $username  = $i->get_request_header('username');
            if ($username == NULl) {
                $response['diagnostic']['status'] = 401;
                $response['diagnostic']['success'] = FALSE;
                $response['diagnostic']['message'] = 'Invalid parameter values!';
                $response['response'] = array('valid' => FALSE);
            }else{
                $where = array('username' => $username);

                $data_user = $this->crud->gda("users", $where);
                $data = $this->crud->gda('publicuser', array('publicUserId' => $data_user['userId']));
                $data['publicPhotoProfile'] = $data['publicPhotoProfile'] != NULL ? 'https://api.biasamata.com/e-law/public/img/uploads/userpics/'.$data['publicPhotoProfile'] : 0;
                $data['accountStatus'] = intVal($data['accountStatus']);
                $data['publicUsername'] = $username;


                $response['diagnostic']['status'] = 200;
                $response['diagnostic']['success'] = TRUE;
                $response['diagnostic']['message'] = 'Success!';
                $response['response'] = $data;
            }
        } else {
            $response['diagnostic']['status'] = 401;
            $response['diagnostic']['success'] = FALSE;
            $response['diagnostic']['message'] = 'Invalid Token !';
            $response['response'] = array('valid' => FALSE);
        }

        $time_end = microtime(true);
        $execution_time = ($time_end - $time_start)/60;
        $response['diagnostic']['elapsed_time'] = ''.$execution_time;
        $response['required'] = $required;

        return $this->response($response);
    }

    public function accountInfo_post(){
        $time_start = microtime(true);
        $response = array();
        $data = array();

        $required = array(
            'authorization' => array('mandatory' => true),
            'publicUserId' => array('mandatory' => true, "type" => 'text', 'example' => 'P-001', 'fieldtext' => 'publicUserId'),
            'fullname' => array('mandatory' => true, "type" => 'text', 'example' => 'Rahmad H Slamet', 'fieldtext' => 'fullname'),
            'email' => array('mandatory' => true, "type" => 'text', 'example' => 'admin@upanastuido.com', 'fieldtext' => 'email'),
            'phoneNumber' => array('mandatory' => true, "type" => 'text', 'example' => '081234567891', 'fieldtext' => 'phoneNumber'),
            'photoProfile' => array('mandatory' => true, "type" => 'file', 'example' => 'arhen.jpg', 'fieldtext' => 'photoProfile'),
        );

        $i = $this->input;
        $publicUserId  = $i->get_request_header('publicUserId');
        $fullname  = $i->get_request_header('fullname');
        $email  = $i->get_request_header('email');
        $phoneNumber  = $i->get_request_header('phoneNumber');

        if ($this->is_token_valid()) {
            if ($publicUserId == NULL OR $fullname == NULL OR $email == NULL) {
                $response['diagnostic']['status'] = 401;
                $response['diagnostic']['success'] = FALSE;
                $response['diagnostic']['message'] = 'Invalid parameter values!';
                $response['response'] = array('valid' => FALSE);
            }else{
                $where = array('publicUserId' => $publicUserId);

                $date = new DateTime();
                $iat = $date->getTimestamp();
                $data = array(
                    'publicFullname' => $fullname,
                    'publicEmail' => $email,
                    'publicContactNumber' => $phoneNumber,
                    'iat' => $iat
                );

                $photoProfile = (isset($_FILES['photoProfile'])) ? $_FILES['photoProfile'] : null;
                if ($photoProfile != null) {
                    // Upload Foto Avatar
                    $data['publicPhotoProfile'] = upload_image('photoProfile', '', 'userpics', '', array());

                    $this->crud->u('publicuser',$data,$where);

                    $response['diagnostic']['status'] = 200;
                    $response['diagnostic']['success'] = TRUE;
                    $response['diagnostic']['message'] = 'Success add Question!';
                }else{
                    $response['diagnostic']['status'] = 200;
                    $response['diagnostic']['success'] = TRUE;
                    $response['diagnostic']['message'] = 'Upload Photo Profile Failed!!';
                    $response['response'] = array('valid' => FALSE);
                }
            }

        } else {
            $response['diagnostic']['status'] = 401;
            $response['diagnostic']['success'] = FALSE;
            $response['diagnostic']['message'] = 'Invalid Token !';
            $response['response'] = array('valid' => FALSE);
        }

        $time_end = microtime(true);
        $execution_time = ($time_end - $time_start)/60;
        $response['diagnostic']['elapsed_time'] = ''.$execution_time;
        $response['required'] = $required;

        return $this->response($response);
    }

    // Private repetited function
    private function is_token_valid()
    {
        $jwt = $this->input->get_request_header('Authorization');
        try {
            $decoded = JWT::decode($jwt, $this->key, array('HS256'));
            return ($this->crud->cw('users', array('username' => $decoded->id, 'userSessionToken' => $jwt)) > 0) ? true : false;
        } catch (\Exception $e) {
            return false;
        }
    }

    private function response($data)
    {
        $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
                ->_display();
        exit();
    }
}
?>
